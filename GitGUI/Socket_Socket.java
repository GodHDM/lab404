//package test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.Buffer;
import java.util.HashMap;

//import org.json.JSONException;
//import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import org.json.*;

public class Socket_Socket {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Thread(new ServerOutput()).start();
		new Thread(new ServerInput()).start();
	}

}

/**
 * 
 * @author Administrator
 * 此类是处理java服务器的输出流的类，需要设备或PHP端建立长连接
 */
class ServerOutput implements Runnable
{

    public static final int PORT = 8000;//监听的端口号
    
    public static HashMap<Integer, Socket> clientList = new HashMap<>();//输出流连接的列表
	
	public void run() {
		// TODO Auto-generated method stub
		ServerSocket serverSocket = null;//定义一个ServerSocket 
		try   
        {    
            serverSocket = new ServerSocket(PORT);//新建一个ServerSocket对象    
            while (true)   
            {   
            	//不断等待新的客户端连接   
                Socket client = serverSocket.accept();
                  
                //客户端连接后，首先读取客户端发来的客户号
                try 
                {
					 //获取客户端输入信息
                	 DataInputStream dis = new DataInputStream(client.getInputStream());
					 //获取客户端输出信息
                	 DataOutputStream dos = new DataOutputStream(client.getOutputStream());
                	 byte[] buffer = new byte[20];
                	 dis.read(buffer);
					 //trim去除字符串两边的空格
                	 Integer clientNum = new Integer(new String(buffer).trim());
                	 
                	 /* 检测clientList中是否存在此对象，若存在，则替换，否则加入 */
                	 if(ServerOutput.clientList.containsKey(clientNum))
                	 {
                		 ServerOutput.clientList.remove(clientNum);
                		 ServerOutput.clientList.put(clientNum, client);
                	 }
                	 else
                		 ServerOutput.clientList.put(clientNum, client);
                	 
                	 System.out.println("Output: 客户端 " + clientNum.toString() + " 连接成功");
                	 dos.write("ok".getBytes());
                	 
                	 /* 0 是本机PHP程序的客户号，此处进行相应的函数处理 */
                	 if(clientNum == 0)
                	 {
                		 
                	 }
                	 
                	 /* 终端发送的消息，此处进行相应的函数处理 */
                	 else 
                	 {
                		 
                	 }
				} 
                catch (Exception e) 
                {
					// TODO: handle exception
				} 
            }    
        }   
        catch (Exception e)   
        {    
            e.printStackTrace();    
        }   
        finally  
        {  
            try   
            {  
                if(serverSocket != null)  
                {  
                    serverSocket.close();  
                }  
            }   
            catch (Exception e)//原来是IOException  
            {  
                e.printStackTrace();  
            }  
        }
	}	
}

/**
 * 本类处理 java端发送到本地 PHP端的消息，发送格式JSON
 * @param deviceNum: 发送过来的设备编号，将设备编号发往PHP端处理
 * @param command: 发送到PHP端的命令
 * @return
 */
class OutputlocalInfoProcess implements Runnable
{
	public Socket local;
	public Integer deviceNum;
	public String command;
	public String filepath;
	public JSONObject localInfo;
	
	public OutputlocalInfoProcess(Socket local, JSONObject localInfo) {
		// TODO Auto-generated constructor stub
		this.local = local;
		try 
		{
			this.deviceNum = Integer.parseInt((String) localInfo.get("deviceNum"));
			this.command = localInfo.getString("command");
			//equals 判断前面对象是否相等，返回值（true,false）
			if(command.equals("0x27"))
			{
				filepath = localInfo.getString("filename");
			}
		} 
		catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Socket client = ServerOutput.clientList.get(0);
		try 
		{
			DataInputStream dis = new DataInputStream(client.getInputStream());
			DataOutputStream dos = new DataOutputStream(client.getOutputStream());
//			JSONObject jsonInfo = new JSONObject();
//			jsonInfo.put("deviceNum", deviceNum);
//			jsonInfo.put("command", command);
			dos.write(localInfo.toString().getBytes());
//			dos.write(("deviceNum:"+deviceNum+"|command:"+command).getBytes());
//			System.out.println(deviceNum + ":" + command);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
//		catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}

/**
 * 本类处理发送到远程设备的消息
 * @param deviceNum: 要发送的设备编号，通过此设备编号寻找发往对应连接的socket
 * @param command: 发往设备的命令
 * @return
 */
class OutputdeviceInfoProcess implements Runnable
{
	public Socket device;
	public Integer deviceNum;
	public String command;
	
	public OutputdeviceInfoProcess(Socket device, Integer deviceNum, String command) {
		// TODO Auto-generated constructor stub
		this.device = device;
		this.deviceNum = deviceNum;
		this.command = command;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		/* 此处先检测玩具是否上线 */
		if( (!(ServerOutput.clientList.containsKey(deviceNum))) || (!(ServerOutput.clientList.get(deviceNum).isConnected())))
		{
			return;
		}
		/* 这里使用发送紧急字节来检测远端是否上线 */
		try {
			ServerOutput.clientList.get(deviceNum).sendUrgentData(0XFF);
			//若没有上线，则抛出异常
		} catch (IOException e) {
			// TODO: handle exception
			return;
		}
		
		/* 若玩具已上线 */
		Socket client = ServerOutput.clientList.get(deviceNum);
		try 
		{
			DataInputStream dis = new DataInputStream(client.getInputStream());
			DataOutputStream dos = new DataOutputStream(client.getOutputStream());
			
			JSONObject jsonInfo = new JSONObject();
			jsonInfo.put("id", deviceNum);
			jsonInfo.put("command", command);
			
			System.out.println(jsonInfo.toString());
			
			/* 检测玩具是否空闲 */
			JSONObject check = new JSONObject();
			JSONObject temp;
			byte[] buffer = new byte[1024];
			check.put("id", deviceNum);
			check.put("command", "0Xff");
			dos.write(check.toString().getBytes());
			dos.flush();
			while (dis.read(buffer) != -1) {
				temp = new JSONObject(new String(buffer));
				if(temp.get("command").equals("0X23")){
					Thread.sleep(3000);					
					dos.write(check.toString().getBytes());
					dos.flush();
					continue;
				}
				if(temp.get("command").equals("0X21"))
				{
					break;
				}
			}
			
			/* 此处根据相应的command，进行相应处理 */
			dos.write(jsonInfo.toString().getBytes());
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return;
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

/**
 * 此类是主要用于socket输入流连接
 * 此链接不需要长连接，只有当客户端有需要传输信息时才连接
 */
class ServerInput implements Runnable
{

    public static final int PORT = 8001;//监听的端口号  

//  public static HashMap<Integer, Socket> clientList = new HashMap<>();
	
	public void run() {
		// TODO Auto-generated method stub
		ServerSocket serverSocket = null;//定义一个ServerSocket  
        try   
        {    
            serverSocket = new ServerSocket(PORT);//新建一个ServerSocket对象    
            while (true)   
            {   
            	//不断等待客户端连接   
                Socket client = serverSocket.accept();
                  
                //客户端连接后，首先读取客户端发来的客户号
                try 
                {
                	 DataInputStream dis = new DataInputStream(client.getInputStream());
                	 DataOutputStream dos = new DataOutputStream(client.getOutputStream());
                	 byte[] buffer = new byte[1024];
                	 dis.read(buffer);
                	 System.out.println(new String(buffer));
                	 JSONObject info = new JSONObject(new String(buffer, "utf-8").trim());
                	 Integer clientNum = Integer.parseInt(info.getString("id"));
//                	 ServerInput.clientList.put(clientNum, client);
                	 System.out.println("Input: 客户端 " + clientNum + " 连接成功");
                	 dos.write("ok".getBytes());
                	 
                	 /* 0 是本机PHP程序的客户号，此处进行相应的函数处理 */
                	 if(clientNum == 0)
                	 {
                     	 new Thread(new InputLocalInfoProcess(client)).start();
                	 }
                	 
                	 /* 终端发送的消息，此处进行相应的函数处理 */
                	 else 
                	 {
                		 new Thread(new InputDeviceInfoProcess(client, clientNum)).start();
                	 }
				} 
                catch (Exception e) 
                {
					// TODO: handle exception
                	e.printStackTrace();
				} 
            }    
        }   
        catch (Exception e)   
        {    
            e.printStackTrace();    
        }   
        finally  
        {  
            try   
            {  
                if(serverSocket != null)  
                {  
                    serverSocket.close();  
                }  
            }   
            catch (Exception e) 
            {  
                e.printStackTrace();  
            }  
        }
	}
	
}

/**
 * 本类处理 本地 PHP端发送过来的消息，并通过输出流方法通知远程设备进行处理
 * @param deviceNum: 本地的PHP端连接
 * @param command: 发送到PHP端的命令
 * @return
 */
class InputLocalInfoProcess implements Runnable
{
	private Socket local;
	
	public InputLocalInfoProcess(Socket local) {
		// TODO Auto-generated constructor stub
		this.local = local;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try 
		{
			DataInputStream dis = new DataInputStream(local.getInputStream());
			DataOutputStream dos = new DataOutputStream(local.getOutputStream());
			byte[] buffer = new byte[1024];
			dis.read(buffer);
			String info = (new String(buffer)).trim();
			/**********/
//			while ((dis.read(buffer))!=-1) {
				
//				System.out.println(new String(buffer).trim());
//				buffer = new byte[1024];
//			}
			/**********/
			JSONObject jsonInfo = new JSONObject(info);
			Integer deviceNum = jsonInfo.getInt("deviceNum");
			String command = jsonInfo.getString("command");
						
			if(command.equals("tell_story"))
			{
				command = "0x05";
			}
			if(command.equals("take_photo"))
			{
				command = "0x11";
			}
			
			/********************/
			/* 测试专用，非JSON格式时 */
//			Integer deviceNum = Integer.parseInt((info.split(":")[0]));
//			String command = info.split(":")[1];
//			System.out.println(info);
			/********************/
			/* 使用输出流通知相应设备进行相应处理  */
			new Thread(new OutputdeviceInfoProcess(ServerOutput.clientList.get(deviceNum), deviceNum, command)).start();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


/**
 * 本类处理远程设备发送过来的消息，并通过输出流方法通知PHP端进行处理
 * @param device: 发送过来的设备的socket连接
 * @param deviceNum: 发往设备的编号
 * @return
 */
class InputDeviceInfoProcess implements Runnable
{
	private Socket device;
	private Integer deviceNum;
	private static final String preFileName = "/var/www/html/ljc/resource/device/";
	
	public InputDeviceInfoProcess(Socket device, Integer deviceNum) {
		// TODO Auto-generated constructor stub
		this.device = device;
		this.deviceNum = deviceNum;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try 
		{
			/*****************/
			/* 测试专用，键盘输入流 */
//			DataInputStream dis = new DataInputStream(System.in);
			/****************/
			DataInputStream dis = new DataInputStream(device.getInputStream());
			DataOutputStream dos = new DataOutputStream(device.getOutputStream());
			byte[] buffer = new byte[1024];
			dis.read(buffer);
			System.out.println("3");
			String info = new String(buffer, "utf-8").trim();
			buffer = new byte[1024];
			JSONObject jsonInfo = new JSONObject(info);
			deviceNum = Integer.parseInt(jsonInfo.getString("id"));
			String command = jsonInfo.getString("command");
			/********************/
			/* 测试专用，非JSON格式时 */
//			deviceNum = Integer.parseInt(info.split(":")[0]);
//			String command = info.split(":")[1];
			/********************/
			dos.write("ok".getBytes());
			System.out.println("4");
			
			/* 此处根据相应的command，进行相应处理 */
			JSONObject localInfo = new JSONObject();
			localInfo.put("deviceNum", deviceNum);
			localInfo.put("command", command);
			
			/* 0x27:接收玩具传输过来的图片文件 */
			if(command.equals("0x27"))
			{
				File filePath = new File(preFileName + deviceNum);
				if(!(filePath.isDirectory()))
				{
					filePath.mkdir();
				}
				
				/* 先接受文件名 */
				dis.read(buffer);
				System.out.println("5");
				String filename = new String(buffer).trim();				
				File file = new File(filePath, filename);
				file.createNewFile();
				RandomAccessFile rf = new RandomAccessFile(file, "rw");
				buffer = new byte[1024];
				dos.write("ok".getBytes());
				System.out.println("6");
				
				/*开始下载文件*/				
				int num = dis.read(buffer);//从此输入流中将最多 buf.length个字节的数据读入一个 buf数组中。返回：读入缓冲区的字节总数			
				while(num != -1)//如果因为已经到达文件末尾而没有更多的数据，则返回 -1
				{
					dos.write("ok".getBytes());
					rf.write(buffer, 0, num);//将buf数组中从0 开始的num个字节写入此文件输出流
					buffer = new byte[1024];
					rf.skipBytes(num);//跳过num个字节数
					num = dis.read(buffer);//从此输入流中将最多 buf.length个字节的数据读入一个 buf数组中。返回：读入缓冲区的字节总数
				}
				localInfo.put("filename", file.getAbsolutePath());
			}
			
			/* 处理完后，使用输出流通知本地进行相应处理 */
			/****************/
			/* 测试专用，显示信息 */
//			System.out.println(jsonInfo.toString());
			/****************/
			
//			new Thread(new OutputlocalInfoProcess(ServerOutput.clientList.get("0), localInfo)).start();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
